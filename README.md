# Clist #

A command line notekeeper.

* *tested on GNU Bash (4.3.11)*
* *it will strive to be portable and small*
* *started off as a personal project(todo-cli) to learn shell scripting*

### Usage Patterns

```
$ clist add "Buy Milk and Cookies"
Entry <f5c9> sucessfully added!

$ clist list
Sorting by Recently Added
[f5c9] (1) 'Buy Milk and Cookies'

$ clist remove F5c9
Error: Entry <F5c9> does not exist.
# entry_id is case sensitive

$ clist branches
[LEAF]
<f5c9> (1) 'Buy Milk and Cookies'
```

## Features
1. [Add, Remove, Prioritize](#add-remove-prioritize)
2. [Listing and Branches](#branches)
3. [Configuration and Options](#comments)

# Add, Remove and Prioritize

## Add
```clist add <entry> <[priority: 1-9]> <branch>```
```
$ clist add "Let's make a tutorial" 5 Tutorial
Entry <4473> successfully added!
```

| Parameter | Description | Example |
| --------- | ----------- | ------- |
| entry | The message, the thing, the crux of the item. **Required** | "Always enclose with double quotes" |
| priority | Assign importance using numbers. *default = 1* | 1 |
| branch | A branch this entry belongs to. *default = leaf* | Tutorial |

## Remove

```
clist remove <entry_id>

```
| Parameter | Description | Example |
| --------- | ----------- | ------- |
| entry_id | An entry's immutable id. **Required** *case-sensitive* | f5c8 


## Prioritize (to be added)

```
clist pri <entry_id> <[priority: 1-9]> 

```
# Listing and Branches

## List

### 1. Recent
```
$ clist list recent
Sorting by Recently Added
[4473] (5) 'Let's make a tutorial'

```
*or if nothing specified it defaults to recent*

```
$ clist list 
Sorting by Recently Added
[4473] (5) 'Let's make a tutorial'
```

### 2. Ascending

```
$ clist add "Bonkers"
Entry <14b9> successfully added!

$ clist list ascend
Sorted by Priority(ascending)
[14b9] (1) 'Bonkers'
[4473] (5) 'Let's make a tutorial
```

### 3. Descending

```
$ clist list descend
Sorted by Priority(descending)
[4473] (5) 'Let's make a tutorial'
[14b9] (1) 'Bonkers'

```

## Branches

### 1. View all Branches
```
$ clist branches
Sorted by Recently Added

[LEAF]
<14b9> (1) 'Bonkers'

[TUTORIAL]
<4473> (5) 'Let's make a tutorial'

```
** All branch names will automatically uppercased **

### 2. Sort Branch by Ascending/Descending priority (to be added)
```
$ clist branches ascend
Sorting by Priority(ascending)

[LEAF]
<14b9> (1) 'Bonkers'
<c3f3> (5) 'I just added one with magic'

[TUTORIAL]
<4473> (5) 'Let's make a tutorial'

$ clist branches descend
Sorting by Priority(descending)

[LEAF]
<c3f3> (5) 'I just added one with magic'
<14b9> (1) 'Bonkers'

[TUTORIAL]
<4473> (5) 'Let's make a tutorial'
```

### 3. Cut a branch (to be added)
```
$ clist cut leaf
Are you sure you want to remove the branch LEAF and all of its entries? (Y/N)
Y
Branch [LEAF] successfully cut down. 4 entries were removed.
```

### 4. Overview of Branches (to be added)
```
$ clist branches -q

[TUTORIAL] (5)
[LEAF] (20)
[GROCERY] (20)
```

# Configuration and Options

Basic configuration is done by editing the `clist` script.
Set  `loc` to a location where you want your named file to be saved
Set  `auto_backup` to True to archive your list before every delete
Set `bdir` to a directory to keep archives

```
$ which clist
/home/user/bin/clist
$ cat /home/user/bin/clist
### Configuration

loc="/home/$USER/notes/list"
auto_backup=True
bdir="/home/$USER/notes/.archives"

### End Configuration
...
```